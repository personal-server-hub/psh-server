'use strict';

// Set environement to test so that the test datasource is used instead of the main datasource.
process.env.RUNTIME_ENV = "test";
// Get test user credentials (used in boot script to create the test users in the test DB)
const apiConfig = require("../../../tdd-user-credentials.json");

const fs = require("fs");
const path = require("path");

let request = require("supertest");
let app;

// Admin auth token placeholder
let adminUserToken;

// TESTS APIs
describe('Testing APIs behaviours...', () => {
    // Clean up previous test db file if necessary
    beforeAll(() => {
        var filePathDb = path.join(__dirname, "../../../sdf-db-test.json");
        if (fs.existsSync(filePathDb)) {
            fs.unlinkSync(filePathDb);
        }
        // Start Loopback API server
        app = require("../../../server/server");
    });

    // Clean up test db file
    afterAll(() => {
        var filePathDb = path.join(__dirname, "../../../sdf-db-test.json");
        if (fs.existsSync(filePathDb)) {
            // Wait a second, otherwise the file is still in use
            setTimeout(() => {
                fs.unlinkSync(filePathDb);
            }, 1000);
        }
    });

    // ******************************* ACL testing
    describe('ACL stuff...', () => {
        test('Login as admin and get auth token', async () => {
            let result = await request(app)
                .post("/api/SdfUsers/login")
                .set("Accept", "application/json")
                .send(apiConfig.adminUser);
            expect(result.res.statusCode).toBe(200);
            const body = JSON.parse(result.res.text);
            expect(body.token.id).not.toBeUndefined();

            adminUserToken = body.id;

        });
    });
});